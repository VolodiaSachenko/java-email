<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<!DOCTYPE HTML>
<html>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
      xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <title>Java Email</title>
</head>
<body>
<div class="container" style="text-align: center;">
    <h1>Email sender</h1>
    <h1 style="position: relative; text-align: center "></h1><br>
</div>
<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form:form action="/send" method="post" modelAttribute="email"
                           cssStyle="width: 500px;position: relative;left: 30%;">
                    <div class="mb-3">
                        <form:label path="email" cssClass="form-label">Email:</form:label>
                        <form:input type="email" path="email" cssClass="form-control" required="required"/>
                    </div>
                    <div class="mb-3">
                        <form:label path="subject" cssClass="form-label">Subject:</form:label>
                        <form:input path="subject" cssClass="form-control" required="required"/>
                    </div>
                    <div class="mb-3">
                        <form:label path="text" cssClass="form-label">Text:</form:label>
                        <form:textarea path="text" cssClass="form-control" required="required"/>
                    </div>
                    <form:button class="btn btn-primary">Send</form:button><br><br>
                </form:form>
            </div>
        </div>
    </div>
</main>
</body>
</html>



