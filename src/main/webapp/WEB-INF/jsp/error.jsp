<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error page</title>
</head>
<body>
<div>
    <h3>Sorry, an exception occurred.</h3><br>
    <h2>${pageContext.errorData.statusCode}</h2>
    <a href="http://localhost:8080/">Visit home page</a>
</div>
</body>
</html>
