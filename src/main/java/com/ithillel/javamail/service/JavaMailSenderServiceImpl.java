package com.ithillel.javamail.service;

import com.ithillel.javamail.model.Email;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class JavaMailSenderServiceImpl implements JavaMailSenderService {
    private final JavaMailSender javaMailSender;

    public JavaMailSenderServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendEmail(Email email) throws MessagingException {
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        helper.setTo(email.getEmail());
        helper.setSubject(email.getSubject());
        helper.setText(email.getText(), true);

        javaMailSender.send(msg);
    }
}
