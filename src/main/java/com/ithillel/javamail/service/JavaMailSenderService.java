package com.ithillel.javamail.service;

import com.ithillel.javamail.model.Email;

import javax.mail.MessagingException;

public interface JavaMailSenderService {
    void sendEmail(Email email) throws MessagingException;
}
