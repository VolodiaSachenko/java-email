package com.ithillel.javamail.controller;

import com.ithillel.javamail.dto.EmailDto;
import com.ithillel.javamail.facade.JavaMailFacade;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.mail.MessagingException;

@Controller
public class JavaMailController {
    private final JavaMailFacade javaMailFacade;

    public JavaMailController(JavaMailFacade javaMailFacade) {
        this.javaMailFacade = javaMailFacade;
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String sendEmailForm(Model model) {
        model.addAttribute("email", new EmailDto());
        return "index";
    }

    @RequestMapping(value = {"/successful-send"}, method = RequestMethod.GET)
    public String showSuccess() {
        return "success";
    }

    @RequestMapping(value = {"/send"}, method = RequestMethod.POST)
    public ModelAndView sendEmail(@ModelAttribute("email") EmailDto emailDto) throws MessagingException {
        javaMailFacade.sendEmail(emailDto);
        return new ModelAndView(new RedirectView("/successful-send"));
    }
}
