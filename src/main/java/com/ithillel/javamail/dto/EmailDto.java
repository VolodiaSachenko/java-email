package com.ithillel.javamail.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class EmailDto {
    private String email;
    private String subject;
    private String text;

    @Override
    public String toString() {
        return "EmailDto{" +
                "email='" + email + '\'' +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailDto emailDto = (EmailDto) o;
        return email.equals(emailDto.email) && subject.equals(emailDto.subject) && text.equals(emailDto.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, subject, text);
    }
}
