package com.ithillel.javamail.util.mapper;

import com.ithillel.javamail.dto.EmailDto;
import com.ithillel.javamail.model.Email;

public final class EmailMapper {

    public static Email fromDto(EmailDto emailDto) {
        Email email = new Email();
        email.setEmail(emailDto.getEmail());
        email.setSubject(emailDto.getSubject());
        email.setText(emailDto.getText());

        return email;
    }
}
