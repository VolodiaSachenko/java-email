package com.ithillel.javamail.facade;

import com.ithillel.javamail.dto.EmailDto;

import javax.mail.MessagingException;

public interface JavaMailFacade {

    void sendEmail(EmailDto emailDto) throws MessagingException;

}
