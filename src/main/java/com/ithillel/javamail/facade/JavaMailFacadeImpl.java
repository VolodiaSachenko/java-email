package com.ithillel.javamail.facade;

import com.ithillel.javamail.dto.EmailDto;
import com.ithillel.javamail.util.mapper.EmailMapper;
import com.ithillel.javamail.model.Email;
import com.ithillel.javamail.service.JavaMailSenderService;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
public class JavaMailFacadeImpl implements JavaMailFacade {
    private final JavaMailSenderService javaMailSender;

    public JavaMailFacadeImpl(JavaMailSenderService javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEmail(EmailDto emailDto) throws MessagingException {
        if (isPresent(emailDto)) {
            Email email = EmailMapper.fromDto(emailDto);
            javaMailSender.sendEmail(email);
        }
    }

    private boolean isPresent(EmailDto emailDto) {
        if (emailDto.getEmail() == null || emailDto.getSubject() == null || emailDto.getText() == null) {
            throw new NullPointerException();
        }
        return true;
    }
}
