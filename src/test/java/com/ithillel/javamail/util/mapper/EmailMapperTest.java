package com.ithillel.javamail.util.mapper;

import com.ithillel.javamail.dto.EmailDto;
import com.ithillel.javamail.model.Email;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.ithillel.javamail.EntityFactory.createEmailDto;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class EmailMapperTest {

    @Test
    public void fromDto() {
        EmailDto emailDto = createEmailDto();
        Email email = EmailMapper.fromDto(emailDto);

        assertEquals(emailDto.getEmail(), email.getEmail());
        assertEquals(emailDto.getSubject(), email.getSubject());
        assertEquals(emailDto.getEmail(), email.getEmail());
    }
}