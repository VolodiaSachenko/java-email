package com.ithillel.javamail;

import com.ithillel.javamail.dto.EmailDto;
import com.ithillel.javamail.model.Email;

public class EntityFactory {

    public static Email createEmail() {
        Email email = new Email();
        email.setEmail("test@email.com");
        email.setSubject("test");
        email.setText("test text");

        return email;
    }

    public static EmailDto createEmailDto() {
        EmailDto emailDto = new EmailDto();
        emailDto.setEmail("test@email.com");
        emailDto.setSubject("test");
        emailDto.setText("test text");

        return emailDto;
    }
}
