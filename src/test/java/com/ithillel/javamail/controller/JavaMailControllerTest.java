package com.ithillel.javamail.controller;

import com.ithillel.javamail.dto.EmailDto;
import com.ithillel.javamail.facade.JavaMailFacade;
import com.ithillel.javamail.facade.JavaMailFacadeImpl;
import com.ithillel.javamail.service.JavaMailSenderServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import static com.ithillel.javamail.EntityFactory.createEmailDto;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class JavaMailControllerTest {
    private EmailDto emailDto;
    @Mock
    private JavaMailFacade javaMailFacade;
    @InjectMocks
    private JavaMailController javaMailController;
    private MockMvc mockMvc;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.emailDto = createEmailDto();
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(
                        new JavaMailController
                                (new JavaMailFacadeImpl(
                                        mock(JavaMailSenderServiceImpl.class))))
                .build();

        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void sendEmailForm() throws Exception {
        String content = javaMailController.sendEmailForm(mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/", content);
    }

    @Test
    public void showSuccess() throws Exception {

        String content = javaMailController.showSuccess();

        assertNotNull(content);

        mockMvcPerformTests("/successful-send", content);
    }

    @Test
    public void sendEmail() throws Exception {
        mockEmailControllerForMockMvc();
        doAnswer(invocationOnMock -> {
            System.out.println("sendEmail() test.");
            return null;
        }).when(javaMailFacade).sendEmail(any());
        javaMailController.sendEmail(emailDto);
        verify(javaMailFacade, times(1)).sendEmail(emailDto);

        mockMvc.perform(post("/send"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    private void mockEmailControllerForMockMvc() {
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(mock(JavaMailController.class))
                .build();
    }

    private void mockMvcPerformTests(String url, String content) throws Exception {
        mockMvc.perform(get(url).content(content))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}