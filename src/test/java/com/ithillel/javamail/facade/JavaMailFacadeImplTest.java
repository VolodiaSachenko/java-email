package com.ithillel.javamail.facade;

import com.ithillel.javamail.dto.EmailDto;
import com.ithillel.javamail.service.JavaMailSenderService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.mail.MessagingException;

import static com.ithillel.javamail.EntityFactory.createEmailDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class JavaMailFacadeImplTest {
    private EmailDto emailDto;
    @Mock
    private JavaMailSenderService javaMailSenderService;
    @InjectMocks
    private JavaMailFacadeImpl javaMailFacade;

    private MockitoSession session;

    @Before
    public void setUp() {
        this.emailDto = createEmailDto();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void sendEmail() throws MessagingException {
        doAnswer(invocationOnMock -> {
            System.out.println("Send email test.");
            return null;
        }).when(javaMailSenderService).sendEmail(any());
        javaMailFacade.sendEmail(emailDto);
        verify(javaMailSenderService, times(1)).sendEmail(any());
    }
}